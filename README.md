# Simple User App 
This is a simple project to demonstrate how to use NodeJS and CassandraDB<br/>
Services:<br/>
1. NodeJS<br/>
2. ExpressJS<br/>
3. Cassandra<br/>
4. Handlebars<br/>



## Configuration
1. Clone repo
2. Do `npm install` from terminal


## Usage
`npm start` <br />



<br /><br /><br />
Yogie Putra - 2017<br />
yogieputra.com
