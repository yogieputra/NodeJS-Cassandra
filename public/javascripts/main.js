$(document).ready(function(){
	$('.deleteuser').on('click', deleteuser);
});

function deleteuser(){
	event.preventDefault();

	var confirmation = confirm('Are you sure?');

	if(confirmation){
		$.ajax({
			type: 'DELETE',
			url: '/subscriber/' + $('.deleteuser').data('id')
		}).done(function(response){
			window.location.replace('/');
		});
	} else {
		return false;
	}
}