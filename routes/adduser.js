var express = require('express');
var router = express.Router();
var cassandra = require('cassandra-driver');

var client = new cassandra.Client({contactPoints: ['127.0.0.1']});
client.connect(function(err, result){
	console.log('adduser: cassandra connected!')
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('adduser', { title: 'Subscribe now!' });
});

var upsertUser = 'INSERT INTO people.users (id, email, first_name, last_name) VALUES (?,?,?,?)';


// POST Add user 
router.post('/', function(req, res){
  var id = cassandra.types.uuid();
  client.execute(upsertUser, [id, req.body.email, req.body.first_name, req.body.last_name], function(err, result){
    if (err){
      res.status(404).send({msg: err});
    } else {
      console.log("Successfully add user!");
      res.redirect('/');
    }
  });
});


module.exports = router;
