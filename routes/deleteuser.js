var express = require('express');
var router = express.Router();
var cassandra = require('cassandra-driver');

var client = new cassandra.Client({contactPoints: ['127.0.0.1']});
client.connect(function(err, result){
	console.log('deleteuser: cassandra connected!')
});


/* DELETE user */
var deleteUserById = 'DELETE FROM people.users WHERE id = ?';
router.get('/:id', function(req, res, next) { 
  console.log('bye ', req.params.id)
  client.execute(deleteUserById, [req.params.id], function(err, result){
    if (err){
      res.status(404).send({msg: err});
    } else {
      res.redirect('/')
    }
  });
});




module.exports = router;
