var express = require('express');
var router = express.Router();
var cassandra = require('cassandra-driver');

var client = new cassandra.Client({contactPoints: ['127.0.0.1']});
client.connect(function(err, result){
	console.log('edituser: cassandra connected!')
});



/* GET Edit user page. */
var getUserById = 'SELECT * FROM people.users WHERE id = ?';
router.get('/:id', function(req, res, next) { 
  console.log('edit: ', req.params.id)
  client.execute(getUserById, [req.params.id], function(err, result){
    if (err){
      res.status(404).send({msg: err});
    } else {
      res.render('edituser', {  
        id: result.rows[0].id,
        email: result.rows[0].email,
        first_name: result.rows[0].first_name,
        last_name: result.rows[0].last_name,
      });
    }
  });
});


// POST Edit user 
var upsertUser = 'INSERT INTO people.users (id, email, first_name, last_name) VALUES (?,?,?,?)';
router.post('/', function(req, res){
  client.execute(upsertUser, [req.body.id, req.body.email, req.body.first_name, req.body.last_name], function(err, result){
    if (err){
      res.status(404).send({msg: err});
    } else {
      console.log("Successfully edit user!");
      res.redirect('/');
    }
  });
});



/* DELETE user */
var deleteUserById = 'DELETE FROM people.users WHERE id = ?';
router.get('/edituser/deleteuser/:id', function(req, res, next) { 
  console.log('bye ', req.params.id)
  client.execute(deleteUserById, [req.params.id], function(err, result){
    if (err){
      res.status(404).send({msg: err});
    } else {
      res.redirect('/')
    }
  });
});





module.exports = router;
