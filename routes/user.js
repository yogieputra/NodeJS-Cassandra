var express = require('express');
var router = express.Router();
var cassandra = require('cassandra-driver');

var client = new cassandra.Client({contactPoints: ['127.0.0.1']});
client.connect(function(err, result){
	console.log('users: cassandra connected!')
});


/* GET home page. */
var getUserById = 'SELECT * FROM people.users WHERE id = ?';
router.get('/:id', function(req, res, next) { 
  console.log(req.params.id)
  client.execute(getUserById, [req.params.id], function(err, result){
  	if (err){
  		res.status(404).send({msg: err});
  	} else {
  		res.render('user', { 
  			title: 'Detail Data',
  			id: result.rows[0].id,
        email: result.rows[0].email,
        first_name: result.rows[0].first_name,
        last_name: result.rows[0].last_name,
  		});
  	}
  });
});

module.exports = router;
